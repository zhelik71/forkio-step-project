Name: 
Forkio Step-project

Description
This is the second DAN-IT step project.

Work were performed by Andriy Zhelizko:

Initializing project and performing gulp task manager.
Develop a Header section for all media queries
Develop People are talking about the fork section

Work was performed by Anna Drozdova:

Develop Revolutionary Editor section
Develop Here is what you get section
Develop Fork Subscription Pricing section

Stack used:

HTML;
CSS;
Npm;
Gulp;
Js;
Sass;
Git;
Node.js;

The project classes were performed under the BEM methodology and all the classes are named accordingly

Installation
This project uses a gulp task manager. To start work with it you must copy the package.json file and install all the dependencies that were used.
In order to do that, you also need to have a node.js installed on your PC.

Usage
In order to go live and do changes you must to put a similar command gulp dev

Authors and acknowledgment
Forkio was performed by two developers - Anna Drozdova and Andriy Zhelizko.